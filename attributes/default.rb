#
# Copyright (c) 2015-2017 Sam4Mobile, 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Create alias for cookbook_name
cookbook_name = 'storm-platform'

# Storm package
default[cookbook_name]['version'] = '1.2.2'
default[cookbook_name]['checksum'] =
  'e97e92ea13998c95ee37565bdf4d6f6a379deb259ec76cb0178cd356502e657f'
default[cookbook_name]['mirror'] =
  'http://archive.apache.org/dist/storm/'
default[cookbook_name]['github'] = # use to fetch files individually
  'https://raw.githubusercontent.com/apache/storm'

# Storm installation
default[cookbook_name]['user'] = 'storm'
default[cookbook_name]['prefix_root'] = '/opt'
default[cookbook_name]['prefix_home'] = '/opt'
default[cookbook_name]['prefix_bin'] = '/opt/bin'
default[cookbook_name]['log_dir'] = '/var/opt/storm/log'
default[cookbook_name]['data_dir'] = '/var/opt/storm/lib'
default[cookbook_name]['java'] = {
  'centos' => 'java-1.8.0-openjdk-headless'
}
default[cookbook_name]['auto_restart'] = true
default[cookbook_name]['unit_path'] = '/etc/systemd/system'

# Cluster configuration
default[cookbook_name]['role'] = cookbook_name
default[cookbook_name]['hosts'] = [] # Use search when empty
default[cookbook_name]['size'] = 1

# Set Nimbus node by its id
default[cookbook_name]['nimbus_id'] = 1 # Between 1 and cluster size
default[cookbook_name]['n_of_nimbus'] = 1 # Number of nimbus (for HA)
default[cookbook_name]['slots_ports'] = [] # Default ports if empty

# Zookeeper configuration
default[cookbook_name]['zookeeper']['role'] = 'zookeeper-storm'
default[cookbook_name]['zookeeper']['hosts'] = [] # Use search when empty
default[cookbook_name]['zookeeper']['size'] = 1

# Storm configuration
# You can use erb notation with chef variables
default[cookbook_name]['config'] = {
  'storm.local.dir' => node[cookbook_name]['data_dir']

  # These may optionally be filled in:
  # List of custom serializations
  # 'topology.kryo.register' => [
  #   'org.mycompany.MyType',
  #   {'org.mycompany.MyType2' => 'org.mycompany.MyType2Serializer'}
  # ],
  # List of custom kryo decorators
  # 'topology.kryo.decorators' => [
  #   'org.mycompany.MyDecorator'
  # ],
  # Metrics Consumers
  # 'topology.metrics.consumer.register' => [
  #   {
  #     'class' => 'backtype.storm.metric.LoggingMetricsConsumer',
  #     'parallelism.hint' => 1
  #   },
  #   {
  #     'class' => 'org.mycompany.MyMetricsConsumer',
  #     'parallelism.hint' => 1,
  #     'argument' => [
  #       {'endpoint' => 'metrics-collector.mycompany.org'}
  #     ]
  #   }
  # ]
}

# Log4j2 configuration, default are loaded directly from xml files located
# in Storm official archive, you can override any default value by setting
# the following two keys. Note: 'configuration' root is not needed
default[cookbook_name]['log4j']['cluster'] = {}
default[cookbook_name]['log4j']['worker'] = {}

# Configure retries for the package resources, default = global default (0)
# (mostly used for test purpose)
default[cookbook_name]['package_retries'] = nil
