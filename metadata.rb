name 'storm-platform'
maintainer 'Make.org'
maintainer_email 'incoming+chef-platform/storm-platform@incoming.gitlab.com'
license 'Apache-2.0'
description 'Install/Configure a Storm cluster'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/storm-platform'
issues_url 'https://gitlab.com/chef-platform/storm-platform/issues'
version '2.2.0'

supports 'centos', '>= 7.1'

chef_version '>= 12.19'

depends 'ark'
depends 'cluster-search'
