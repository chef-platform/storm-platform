#
# Copyright (c) 2015-2017 Sam4Mobile, 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'
# rubocop:disable Metrics/BlockLength

nimbus_seeds = %w[storm-kitchen-01.kitchen storm-kitchen-02.kitchen]

# Waiting for the services to be up
wait_services([['nimbus', 6627], ['ui', 8080]])

describe 'Storm Nimbus Daemon' do
  it 'is running' do
    expect(service('storm-nimbus')).to be_running
  end

  it 'is launched at boot' do
    expect(service('storm-nimbus')).to be_enabled
  end

  it 'is listening on correct port' do
    expect(port(6627)).to be_listening
  end

  remote_conf = '/opt/storm/bin/storm remoteconfvalue'

  it 'has the correct server list' do
    res = `#{remote_conf} storm.zookeeper.servers`
    expect(res).to eq("storm.zookeeper.servers: zookeeper-storm.kitchen\n")
  end

  it 'has the correct local directory' do
    res = `#{remote_conf} storm.local.dir`
    expect(res).to eq("storm.local.dir: /var/opt/storm/lib\n")
  end

  it 'has the correct nimbus seeds' do
    res = `#{remote_conf} nimbus.seeds`
    exp = "nimbus.seeds: #{nimbus_seeds.join(' ')}\n"
    expect(res).to eq(exp)
  end
end

describe 'Storm UI Daemon' do
  it 'is running' do
    expect(service('storm-ui')).to be_running
  end

  it 'is launched at boot' do
    expect(service('storm-ui')).to be_enabled
  end

  it 'is listening on correct port' do
    expect(port(8080)).to be_listening
  end

  curl = 'http_proxy="" curl -sS -X GET'
  url = 'http://localhost:8080'

  it 'returns no error' do
    result = `#{curl} #{url}/api/v1/cluster/summary`
    exp = /\{"error":(.+)\}/
    error = exp.match(result)
    expect(error).to be_nil
  end

  it 'exhibits the correct Nimbus seeds' do
    result = `#{curl} #{url}/api/v1/cluster/configuration`
    exp = /.*"nimbus.seeds":\[\"#{nimbus_seeds.join('","')}\"\].*/
    expect(result).to match(exp)
  end

  it 'exhibits the correct Zookeeper list' do
    result = `#{curl} #{url}/api/v1/cluster/configuration`
    exp = /.*"storm.zookeeper.servers":\["zookeeper-storm.kitchen"\].*/
    expect(result).to match(exp)
  end
end

# rubocop:enable Metrics/BlockLength
