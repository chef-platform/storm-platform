#
# Copyright (c) 2015-2017 Sam4Mobile, 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

describe 'Storm Nimbus Daemon' do
  it 'is NOT running' do
    expect(service('storm-nimbus')).to_not be_running
  end

  it 'is NOT launched at boot' do
    expect(service('storm-nimbus')).to_not be_enabled
  end

  it 'is NOT listening on correct port' do
    expect(port(6627)).to_not be_listening
  end
end

describe 'Storm UI Daemon' do
  it 'is NOT running' do
    expect(service('storm-ui')).to_not be_running
  end

  it 'is NOT launched at boot' do
    expect(service('storm-ui')).to_not be_enabled
  end

  it 'is NOT listening on correct port' do
    expect(port(8080)).to_not be_listening
  end
end
