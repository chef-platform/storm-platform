#
# Copyright (c) 2015-2017 Sam4Mobile, 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'serverspec'
set :backend, :exec

require 'English'

def wait_services(services)
  services.each do |service, port|
    wait_for(30, service, "journalctl -u storm-#{service} | grep Running")
    wait_for(10, port, "ss -tunl 2> /dev/null | grep -- :#{port}")
  end
end

def wait_for(tries, something, cmd)
  (1..tries).each do |try|
    `#{cmd}`
    break if $CHILD_STATUS.exitstatus.zero?
    puts "Waiting for #{something}… Try ##{try}/#{tries}, waiting #{try}s"
    sleep(try)
  end
end
